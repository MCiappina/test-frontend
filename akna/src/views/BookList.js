import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import LoadingSpinner from "../components/LoadingSpinner";

import logo from "../resources/akna_logo.png";

import Service from "../services/service";

export default function BookList() {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [bookList, setBookList] = useState([]);

  // componentWillMount to get booksList
  useEffect(() => {
    getBookList();
  }, []);

  const getBookList = () => {
    setLoading(true);
    Service.getBookList()
      .then((response) => {
        setBookList(response.data);
        setLoading(false);
      })
      .catch((err) => console.log(err));
  };

  const handleDelete = (itemId) => {
    setLoading(true);
    Service.deleteBook(itemId)
      .then(() => {
        getBookList();
      })
      .catch((err) => console.log(err));
  };

  const handleEdit = (item) => {
    history.push({ pathname: `/bookform/${item.id}`, state: { book: item } });
  };

  return (
    <div>
      {loading ? (
       
        <LoadingSpinner />
        
      ) : (
        <div className="row">
          <div className="col-12 d-flex justify-content-center">
            <img src={logo} className="img-fluid" alt="akna logo" />
          </div>
          <div className="col-12 d-flex justify-content-center">
            <table className="mx-5 my-5 col col-xs-6 overflow-auto table table-striped">
              <thead>
                <tr>
                  <th scope="col">Nome</th>
                  <th scope="col">Autor</th>
                  <th scope="col">Preço</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {bookList.map((item) => {
                  return (
                    <tr key={item.id}>
                      <td>{item.name}</td>
                      <td>{item.author}</td>
                      <td>${item.price}</td>
                      <td>
                        <div className="dropdown">
                          <button
                            className="btn btn-secondary dropdown-toggle"
                            type="button"
                            id="dropdownMenu2"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                          >
                            Opções
                          </button>
                          <div
                            className="dropdown-menu"
                            aria-labelledby="dropdownMenu2"
                          >
                            <button
                              onClick={() => handleEdit(item)}
                              className="dropdown-item"
                              type="button"
                            >
                              Editar
                            </button>
                            <button
                              onClick={() => handleDelete(item.id)}
                              className="dropdown-item"
                              type="button"
                            >
                              Excluir
                            </button>
                          </div>
                        </div>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      )}
    </div>
  );
}

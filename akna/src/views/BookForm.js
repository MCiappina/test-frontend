import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import LoadingSpinner from "../components/LoadingSpinner";

import logo from "../resources/akna_logo.png";

import Service from "../services/service";

export default function BookForm(props) {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [book, setBook] = useState({
    name: "",
    author: "",
    price: "",
  });

  useEffect(() => {
    if (props.location.state) {
      setBook(props.location.state.book);
    } else {
      return null;
    }
  }, []);

  const handleInput = (event) => {
    let input = event.target.value;
    setBook({ ...book, [event.target.name]: input });
  };

  const updateBook = () => {
    Service.updateBook(book.id, book)
      .then(() => {
        history.push("/booklist");
      })
      .catch((err) => console.log(err));
  };

  const postBook = () => {
    Service.postBook(book)
      .then(() => {
        history.push("/booklist");
      })
      .catch((err) => console.log(err));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setLoading(true);
    if (book.id) {
      updateBook();
    } else {
      postBook();
    }
  };

  return (
    <div className="h-100">
      {loading ? (
        <LoadingSpinner />
      ) : (
        <div className="row d-flex-">
          <div className="col-12 d-flex justify-content-center">
            <img src={logo} className="img-fluid" alt="akna logo" />
          </div>

          <form
            className="book-form rounded w-100 p-5 my-2 mx-auto d-flex custom-width flex-column justify-content-center"
            onSubmit={handleSubmit}
          >
            <div className="mb-3">
              <label htmlFor="name" className="form-label">
                Nome
              </label>
              <input
                onChange={handleInput}
                type="text"
                className="form-control"
                name="name"
                value={book.name}
                required
              />
            </div>
            <div className="mb-3">
              <label htmlFor="author" className="form-label">
                Autor
              </label>
              <input
                onChange={handleInput}
                type="text"
                className="form-control"
                name="author"
                value={book.author}
                required
              />
            </div>
            <div className="mb-3">
              <label htmlFor="price" className="form-label">
                Preço
              </label>
              <input
                onChange={handleInput}
                type="number"
                className="form-control"
                name="price"
                value={book.price}
                required
              />
            </div>
            <button type="submit" className="w-100 mx-auto mt-2 custom-width btn submit-btn">
              SUBMIT
            </button>
          </form>
        </div>
      )}
    </div>
  );
}

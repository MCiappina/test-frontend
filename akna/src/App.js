import React from 'react';

import "./App.css";
import { Switch, Route, BrowserRouter as Router, Redirect } from "react-router-dom";
import BookList from "./views/BookList";
import BookForm from './views/BookForm';
import Navbar from './components/Navbar';
import Footer from './components/Footer';

function App() {
  return (
    <div className="container d-flex flex-column justify-content-space-between">
    
      <Router>
      <Navbar />
        <Switch>
          <Route
            exact
            path="/"
            render={() => {
              return (<Redirect to="/booklist" />)
            }}
          />
          <Route path="/booklist" component={BookList} />
          <Route path="/bookform" component={BookForm} />
          <Route path="/bookform/:book" component={BookForm} />
        </Switch>
        <Footer />
      </Router>
    </div>
  );
}

export default App;

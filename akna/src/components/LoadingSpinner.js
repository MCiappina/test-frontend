import React from "react";

export default function LoadingSpinner() {
  return (
    <div className="loading-spinner mt-5 d-flex justify-content-center align-items-center flex-grow-1">
    <div className="spinner-grow" role="status">
      <span className="visually-hidden">Loading...</span>
      </div>
    </div>
  );
}

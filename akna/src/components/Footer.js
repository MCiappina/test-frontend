import React from "react";

export default function Footer() {
  return (
    <div class="footer mt-auto">
        <div class="row">
          <div class="col-md-12 text-center">
            <p>Matheus Ciappina 2021</p>
          </div>
      </div>
    </div>
  );
}

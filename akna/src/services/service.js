import Api from "./api";

export default {
    getBookList() {
        return Api().get("books");
    },
    deleteBook(id) {
        return Api().delete(`books/${id}`);
    },
    updateBook(id, data) {
        return Api().put(`books/${id}`, data)
    },
    postBook(data){
        return Api().post("books", data)
    }
};
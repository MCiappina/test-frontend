# AKNA Challenge

## Objective

Develop an app for listing books and inserting new books according to the specifications: https://bitbucket.org/akna/test-frontend/src/master/

## Instructions

To run this project:

1.  Download this repository.
2.  Type the following commands using the terminal, inside the project folder
3.  `cd akna`
4.  Create a .env file and paste " _REACT_APP_API_HOST="http://localhost:5000"_ " to it.
5.  `npm install -g json-server `
6.  `npm install`
7.  `npm run server`
8.  Finally run `npm start`.

## Technologies Used

- Node.js
- React (with hooks)
- CSS
- Bootstrap
- HTML
- Axios
- React-Router
